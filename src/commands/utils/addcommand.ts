import { Command } from "../../types/index";

export const addcommand: Command = {
  aliases: ["addcommand"],
  description: "Add custom commands",
  usage: "<message>",
  disabled: true,
  async run(client, msg, args) {},
};
