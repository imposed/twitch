import { Command } from "../../types/index";

export const ping: Command = {
  aliases: ["ping"],
  description: "Checks if the bot is alive",
  usage: "<message>",
  disabled: false,
  async run(client, msg, args) {
    client.say(msg.channelName, "Pong!");
  },
};
