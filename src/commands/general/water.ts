import { Command } from "../../types/index";

let num = 1;

export const water: Command = {
  aliases: ["water", "fill"],
  description: "Check how many times splashley has filled up her water",
  usage: "<message>",
  disabled: false,
  async run(client, msg, args) {
    client.say(
      msg.channelName,
      `FeelsGoodMan Splashey has refilled her water ${num++} time${
        num <= 2 ? "" : "s"
      } FeelsGoodMan`
    );
  },
};
