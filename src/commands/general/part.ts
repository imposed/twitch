import { Command } from "../../types/index";

export const part: Command = {
  aliases: ["part"],
  description: "Leaves the channel the command is executed.",
  usage: "<message>",
  disabled: false,
  async run(client, msg, args) {
    client.say(
      msg.channelName,
      "Ok I am leaving the channel BibleThump BibleThump"
    );
    client.part(msg.channelName);
    console.log(`Parted ${msg.channelName}`);
  },
};
