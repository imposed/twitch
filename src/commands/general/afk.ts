import { Command } from "../../types/index";

let num = 1;

export const afk: Command = {
  aliases: ["afk", "brb"],
  description: "Check how many times penguhs has went brb",
  usage: "<message>",
  disabled: false,
  async run(client, msg, args) {
    client.say(
      msg.channelName,
      `FeelsGoodMan penguhs has went afk ${num++} time${
        num <= 2 ? "" : "s"
      } FeelsGoodMan`
    );
  },
};
