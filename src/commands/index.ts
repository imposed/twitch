import { Command } from "../types/index";
import { ping, part, water, afk } from "./general";
import { addcommand } from "./utils/addcommand";

export const commands: Command[] = [ping, part, water, afk, addcommand];

const commandAliases = commands.reduce((all, command) => {
  const aliases = [...new Set(command.aliases)];

  return aliases.reduce((previous, commandName) => {
    return { ...previous, [commandName]: command };
  }, all);
}, {} as Record<string, Command>);

export const aliases = new Map<string, Command>(Object.entries(commandAliases));

const allCommandAliases = commands.map((c) => c.aliases).flat();

const duplicateAliases = allCommandAliases.filter(
  (c, i, a) => a.indexOf(c) !== i
);

if (duplicateAliases.length > 0) {
  throw new Error(
    `Encountered duplicate aliases: ${duplicateAliases.join(", ")}`
  );
}
