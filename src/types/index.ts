import { ChatClient, PrivmsgMessage } from "dank-twitch-irc";

export interface Command {
  description?: string;

  usage?: string;

  aliases: string[];

  disabled: boolean;

  run(
    client: ChatClient,
    msg: PrivmsgMessage,
    args: string | string[]
  ): Promise<void>;
}
