require("dotenv").config();

import { ChatClient } from "dank-twitch-irc";
import config from "./config";
import { aliases, commands } from "./commands";

const prefix = "!";

const client = new ChatClient(config);

client.on("ready", () => console.log("Successfully connected to chat"));

client.on("PRIVMSG", async (msg) => {
  if (!msg.messageText.startsWith(prefix)) return;

  const [rawCommand, ...args] = msg.messageText.replace(prefix, "").split(" ");
  const commandName = rawCommand.toLowerCase();

  const command = aliases.get(commandName);

  if (!command) return;

  if (command.disabled === true) {
    return client.say(msg.channelName, "This command is currently disabled");
  }

  await command.run(client, msg, args);
});

client
  .connect()
  .then(() => client.join("penguhs"))
  .catch((e) => console.log(e));
