interface iClientConfig {
  rateLimits?: {
    highPrivmsgLimits?: number;
    lowPrivmsgLimits?: number;
  };
  connection?: {
    type?: string;
    secure?: boolean;
    host?: string;
    port?: number;
    stream?: any;
    preSetup: boolean;
  };
  maxChannelCountPerConnection?: number;
  connectionRateLimits?: {
    parallelConnections: number;
    releaseTime: number;
  };
  requestMembershipCapability: boolean;
  installDefaultMixins: boolean;
  ignoreUnhandledPromiseRejections: boolean;
}

export default {
  username: "shugnep",
  // password: process.env.PASSWORD as string,
  password: "oauth:9ztpnea516j0dynywau35jaidx4pbn",
};
